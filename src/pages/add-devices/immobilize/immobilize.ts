import { Component, Renderer } from "@angular/core";
import { NavParams, ViewController, IonicPage } from "ionic-angular";

@IonicPage()
@Component({
    selector: 'page-immobilize',
    templateUrl: './immobilize.html'
})

export class ImmobilizePage {
    dataParam: any;
    respData: any;
    msg: any;

    constructor(
        params: NavParams,
        public renderer: Renderer,
        public viewCtrl: ViewController,
        // private fb: FormBuilder,
      ) {
        this.renderer.setElementClass(viewCtrl.pageRef().nativeElement, 'my-popup', true);
        this.dataParam = params.get("param");
        this.respData = params.get("udata");
        this.msg = params.get("msg");
      }

      dismiss() {
          this.viewCtrl.dismiss();
      }

      yes() {

      }

      no() {
        this.viewCtrl.dismiss();
      }
      
}
