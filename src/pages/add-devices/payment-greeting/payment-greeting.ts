import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-payment-greeting',
  templateUrl: 'payment-greeting.html',
})
export class PaymentGreetingPage {
  paymentTime = new Date()
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {

    console.log('ionViewDidLoad PaymentGreetingPage');
    this.paymentTime;
  }
}
