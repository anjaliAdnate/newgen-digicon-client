import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FuelConsumptionPage } from './fuel-consumption';
import { SelectSearchableModule } from 'ionic-select-searchable';

@NgModule({
  declarations: [
    FuelConsumptionPage,
  ],
  imports: [
    IonicPageModule.forChild(FuelConsumptionPage),
    SelectSearchableModule
  ],
})
export class FuelConsumptionPageModule {}
