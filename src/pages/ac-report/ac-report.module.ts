import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AcReportPage, ACDetailPage } from './ac-report';
import { SelectSearchableModule } from 'ionic-select-searchable';

@NgModule({
  declarations: [
    AcReportPage,
    ACDetailPage
  ],
  imports: [
    IonicPageModule.forChild(AcReportPage),
    SelectSearchableModule
  ],
  entryComponents: [ACDetailPage]
})
export class AcReportPageModule {}
