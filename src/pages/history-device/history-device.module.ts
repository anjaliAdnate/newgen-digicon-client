import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HistoryDevicePage } from './history-device';
import { IonBottomDrawerModule } from '../../../node_modules/ion-bottom-drawer';
import { SelectSearchableModule } from 'ionic-select-searchable';

@NgModule({
  declarations: [
    HistoryDevicePage,
  ],
  imports: [
    IonicPageModule.forChild(HistoryDevicePage),
    IonBottomDrawerModule,
    SelectSearchableModule
  ],
  // providers: [DatePicker]
})
export class HistoryDevicePageModule {}
