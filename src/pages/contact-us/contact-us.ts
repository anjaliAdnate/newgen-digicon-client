import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiServiceProvider } from '../../providers/api-service/api-service';


@IonicPage()
@Component({
  selector: 'page-contact-us',
  templateUrl: 'contact-us.html',
})
export class ContactUsPage {

  contact_data: any = {};
  contactusForm: FormGroup;
  submitAttempt: boolean;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public api: ApiServiceProvider,
    public toastCtrl: ToastController) {
    this.contactusForm = formBuilder.group({
      name: ['', Validators.required],
      mail: ['', Validators.email],
      // mobile: ['', Validators.required],
      // title: ['', Validators.required],
      note: ['', Validators.required]
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ContactUsPage');
  }


  contactUs() {
    this.submitAttempt = true;
    console.log(this.contactusForm.value)
    if (this.contactusForm.valid) {
      
      this.contact_data = {
        "email": this.contactusForm.value.mail,
        // "title": this.contactusForm.value.title,
        "msg": this.contactusForm.value.note,
        // "phone": this.contactusForm.value.mobile,
        "dealerid": "rudragpstrack@gmail.com",
        "dealerName": "Sashi Prakash"
      }
      this.api.startLoading().present();
      this.api.contactusApi(this.contact_data)
        .subscribe(data => {
          this.api.stopLoading();
          console.log(data.message)
          let toast = this.toastCtrl.create({
            message: 'Your message has been sent, we will get back to you soon..',
            position: 'bottom',
            duration: 2000
          });

          toast.onDidDismiss(() => {
            console.log('Dismissed toast');
            this.navCtrl.setRoot(ContactUsPage);
          });

          toast.present();
        },
          error => {
            this.api.stopLoading();
            console.log(error);
          });
    }
  }

}
