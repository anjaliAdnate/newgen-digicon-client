export class URLS {
    usersURL: string = "https://login.vtstrack.co.in/users/";
    devicesURL: string = "https://login.vtstrack.co.in/devices";
    gpsURL: string = "https://login.vtstrack.co.in/gps";
    geofencingURL: string = "https://login.vtstrack.co.in/geofencing";
    trackRouteURL: string = "https://login.vtstrack.co.in/trackRoute";
    groupURL: string = "https://login.vtstrack.co.in/groups/";
    notifsURL: string = "https://login.vtstrack.co.in/notifs";
    stoppageURL: string = "https://login.vtstrack.co.in/stoppage";
    summaryURL: string = "https://login.vtstrack.co.in/summary";
    shareURL: string = "https://login.vtstrack.co.in/share";
    usertripURL: string = "https://login.vtstrack.co.in/user_trip/";
    notifioURL: string = "https://login.vtstrack.co.in/notifIO";
    dev_url: string = "https://login.vtstrack.co.in";
    poi_url: string = "https://login.vtstrack.co.in/poi";
    mainURL: string = "https://login.vtstrack.co.in";
    // http://13.127.197.95:3000/user_trip/trip_detail?&uId=5b31e85d2b8fc936ea1cbff9&from_date=2019-03-29T18:30:00.060Z&to_date=2019-03-30T17:38:24.997Z
}
